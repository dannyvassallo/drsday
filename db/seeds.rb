 require 'faker'

 # Create Posts
 100.times do
   Micropost.create!(
     drname:     Faker::Name.name,
     first_name: Faker::Name.first_name,
     last_name:  Faker::Name.last_name,
     content:    Faker::Lorem.sentence
   )
 end
 notes = Micropost.all

 puts "Seed finished"
 puts "#{Micropost.count} notes created"
