require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'csv'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Ilmd
  class Application < Rails::Application
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
    config.secret_key_base = Figaro.env.secret_key_base
    config.assets.initialize_on_precompile = false
    config.action_dispatch.default_headers.merge!({'X-Frame-Options' => 'ALLOWALL'})
  end
end

