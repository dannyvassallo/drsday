class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include AnalyticsHelper

  def admin?
    current_user.try(:admin?)
  end
  helper_method :admin?

  def authorize
    unless admin?
      flash[:error] = "unauthorized access"
      redirect_to home_path
      false
    end
  end

  protected
  def authenticate_user!
    redirect_to root_path, notice: "You must sign in with Facebook first!" unless user_signed_in?
  end

end
