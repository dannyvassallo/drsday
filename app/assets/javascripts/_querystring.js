
//Get fngt appdata
var urlParams;
(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
})();

var app_data = urlParams["app_data"];

if (app_data) {
  //Parses fngt appdata into JSON hash
  var params = $.parseJSON(app_data);

  //Turns hash keys and values into arrays
  var array_keys = new Array();
  var array_values = new Array();

  for (var key in params) {
      array_keys.push(key);
      array_values.push(params[key]);
  }
  //rebuilds url
  var url = document.location.host;
  var redirectUrl = "http://"+url+"?"+array_keys[0]+"="+array_values[0]+
                 "&"+array_keys[1]+"="+array_values[1]+
                 "&"+array_keys[2]+//this index has no properties in this campaign
                 "&"+array_keys[3]+"="+array_values[3]+
                 "&"+array_keys[4]+"="+array_values[4];
  //redirects with proper utm set up
  location.href = redirectUrl;
}
