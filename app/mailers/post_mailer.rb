class PostMailer < ActionMailer::Base
  def posted_confirmation(micropost)
    @micropost = micropost
    mail :to => ENV['NOTE_EMAIL'], :from => "noreply@drsday.herokuapp.com", :subject => "DrDay15: Someone Posted a Note!"
  end
end
